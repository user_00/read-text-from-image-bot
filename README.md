# Read Text From Image Bot

## What is it?

This is a simple telegram bot for reading English or Russian text from a sent picture.

## Getting Started (without docker)

### Preparing

#### Install python 3

##### Windows:

[download python](https://www.python.org/downloads/).

##### Linux:

```shell
$ sudo apt-get install python
```

#### Install dependencies

##### Windows:

*Run these commands via `cmd.exe`.*

Initialization and launch of the virtual environment.

```shell
> python3 -m venv venv

> cd venv/Scripts

> activate.bat

> cd ../..
```

Install dependencies.

```shell
> pip install -r requirements.txt
```

##### Linux:

Installing the python virtual environment.

```shell
$ sudo apt-get install python3-venv
```

Initialization and launch of the virtual environment.

```shell
$ python3 -m venv venv

$ source venv/bin/activate
```

Install dependencies.

```shell
$ pip install -r requirements.txt
```

#### Install tesseract

##### Windows:

[download tesseract](https://tesseract-ocr.github.io/tessdoc/).

After installation, you need to specify the path to tesseract cmd in the `path_to_tesseract_cmd`
variable in the` config.py` file.

The standard path — _"C:\Program Files\Tesseract-OCR\tesseract.exe"_.

##### Linux:

```shell
$ sudo apt-get install tesseract-ocr
```

#### Install dictionaries for tesseract

##### Windows:

[Download dictionaries for tesseract](https://tesseract-ocr.github.io/tessdoc/Data-Files).

Download the Russian dictionary `rus.traineddata` and move it to the folder` Tesseract-OCR\tessdata`.

The standard path — _"C:\Program Files\Tesseract-OCR\tessdata"_.

##### Linux:

```shell
$ sudo apt-get install tesseract-ocr-rus
```

[Learn more about installing dictionaries for linux](https://askubuntu.com/questions/793634/how-do-i-install-a-new-language-pack-for-tesseract-on-16-%20%2004).

#### Telegram API key

Create file `api_key_telegram.txt` in project directory and put your telegram API key in it.

### Launch

Start virtual environment if not already (see paragraph _"Install dependencies"_)
and start command:

```shell
python main.py
```

### Stopping

##### Windows:

```shell
> ctrl + c
> cd venv/Scripts
> deactivate.bat
```

##### Linux:

```shell
$ ctrl + c
$ deactivate
```

## Getting Started (with docker)

### Preparing

#### Install docker

[Install docker](https://www.docker.com/get-started) on your OS.

#### Telegram API key

Create file `api_key_telegram.txt` in project directory and put your telegram API key in it.

### Launch

```shell
 docker build -t read_text_from_image_bot .

 docker run --name rtfi_bot --rm -d read_text_from_image_bot
```

### Stopping

```shell
docker stop rtfi_bot
```

## Structure

- `logo` — bot logos.
- `photos` — a folder that temporarily stores photos sent by users.
- `api_key_telegram.txt` — a file with an API key for the telegram bot, 
  the user creates it himself and places his key there.
- `config.py`— config file
- `imports.py` — a file with imports of the necessary libraries for the main script to work
- `main.py` — the main script
- `requirements.txt` — a file indicating the required dependencies and their versions
- `Dockerfile` — docker file 
- `.gitignore` — description of which files to ignore when committing
- `README.md` — this is what you are reading now :)

## Authors

* **Nelin Maxim** - [GitLab](https://gitlab.com/user_00)

## License

This project is licensed under the MIT License.
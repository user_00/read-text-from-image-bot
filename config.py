def get_api_key_telegram():
    with open('api_key_telegram.txt', 'r', encoding='utf-8') as f:
        return f.read().replace('\n', '')


# Tesseract path (only for windows)
path_to_tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

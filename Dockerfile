FROM python:3.8

RUN mkdir --p /usr/src/app/
WORKDIR /usr/src/app/
COPY . /usr/src/app/

RUN apt-get update
RUN apt-get install tesseract-ocr -y
RUN apt-get install tesseract-ocr-rus -y
RUN pip install -r requirements.txt

# For open-cv to work in docker, you need additional dependencies
RUN pip install opencv-python-headless
RUN pip install opencv-contrib-python-headless

ENV TZ Europe/Moscow

CMD ["python", "main.py"]
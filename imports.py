import cv2
import re
import os
import pytesseract

from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor

import platform
from config import *

# Tesseract connection path (only for windows)
if platform.system() == 'Windows':
    pytesseract.pytesseract.tesseract_cmd = path_to_tesseract_cmd

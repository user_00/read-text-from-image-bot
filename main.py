from imports import *
from config import *

bot = Bot(token=get_api_key_telegram())
dp = Dispatcher(bot)

lang = 'rus'
line_breaks = False


@dp.message_handler(commands=['start'])
async def send_welcome(message: types.Message):
    btn_rus_lb = types.KeyboardButton('Русский с переносами')
    btn_rus = types.KeyboardButton('Русский без переносов')
    btn_eng_lb = types.KeyboardButton("Английский с переносами")
    btn_eng = types.KeyboardButton('Английский без переносов')
    markup_reply = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup_reply.add(btn_rus_lb, btn_rus, btn_eng_lb, btn_eng)
    await message.reply('Привет.\n'
                        'Отправь картинку для считывания с неё русского или английского текстов, но сначала '
                        'выбери режим считывания.', reply_markup=markup_reply)


@dp.message_handler(commands=['help'])
async def send_welcome(message: types.Message):
    await message.reply('Отправь картинку для считывания с неё русского или английского текстов, но сначала '
                        'выбери режим считывания. \n'
                        '\n'
                        'Доступные режимы считывания: \n'
                        'Русский с переносами — распознаёт русский язык с фото и оставляет все переносы'
                        ' строк как на картинке;\n'
                        'Русский без переносов — распознаём русский язык с фото и убирает все переносы строк;\n'
                        'Английский с переносами — распознаёт английский язык с фото и оставляет все переносы'
                        ' строк как на картинке;\n'
                        'Английский без переносов — распознаём английский язык с фото и убирает все переносы строк.\n')


@dp.message_handler(content_types=['text'])
async def echo_all(message: types.Message):
    global lang
    global line_breaks
    if message.text == 'Русский с переносами':
        lang = 'rus'
        line_breaks = True
    elif message.text == 'Русский без переносов':
        lang = 'rus'
        line_breaks = False
    elif message.text == 'Английский с переносами':
        lang = 'eng'
        line_breaks = True
    elif message.text == 'Английский без переносов':
        lang = 'eng'
        line_breaks = False
    else:
        await message.reply('Вставьте картинку.')


@dp.message_handler(content_types=['document'])
async def handle_docs_document(message: types.Message):
    await message.reply('Вставьте картинку, а не файл.')


@dp.message_handler(content_types=['photo'])
async def handle_docs_photo(message: types.Message):
    global lang
    global line_breaks

    try:
        # Save the image
        file_id = message.photo[len(message.photo) - 1].file_id
        file_info = await bot.get_file(file_id)
        downloaded_file = await bot.download_file(file_info.file_path)
        path = file_info.file_path
        with open(path, 'wb') as new_file:
            new_file.write(downloaded_file.getvalue())

        # Read the image and convert it to RGB
        image = cv2.imread(path)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        # Read text from image
        config = r'--oem 3 --psm 6'
        text_from_image = pytesseract.image_to_string(image, config=config, lang=lang)

        if not line_breaks:
            text_from_image = re.sub("^\s+|\n|\r|\s+$", ' ', text_from_image)

        await message.reply(text_from_image)

        # Delete the image
        if os.path.isfile(path):
            os.remove(path)


    except Exception as e:
        await message.reply('Что-то пошло не так :(' + '\n' + str(e))


if __name__ == '__main__':
    executor.start_polling(dp)
